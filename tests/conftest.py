import subprocess

import pytest

import helixd


@pytest.fixture(autouse=True)
def clean_before_and_after():
    def clean():
        subprocess.run("lxc stop test-instance", shell=True)
        subprocess.run("lxc delete test-instance", shell=True)
        subprocess.run("lxc profile delete test-profile", shell=True)
        subprocess.run("lxc network delete test-network", shell=True)
        subprocess.run("lxc storage volume delete test-storage-pool test-volume", shell=True)
        subprocess.run("lxc storage delete test-storage-pool", shell=True)

    clean()
    yield  # see https://stackoverflow.com/questions/22627659/run-code-before-and-after-each-test-in-py-test
    clean()


@pytest.fixture(scope="module")
def client():
    return helixd.Client()
